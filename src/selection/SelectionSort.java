package selection;

public class SelectionSort {

	public static void main(String[] args) {
		
		SelectionSort ss=new SelectionSort();
		int[] nums={55,3,12,1,65,33,2};
		
		ss.selectionSort(nums);
		for(int i:nums) {
			System.out.print(i+" ");
		}
	}

	private void selectionSort(int[] nums) {
		
		int len=nums.length;
		for(int i=0;i<len-1;i++) {
			int tmpIdx=i;
			for(int j=tmpIdx+1;j<len;j++) {
				if(nums[j]<nums[tmpIdx]) {
					tmpIdx=j;
				}
			}
			if(tmpIdx!=i) {
				int tmpNum=nums[i];
				nums[i]=nums[tmpIdx];
				nums[tmpIdx]=tmpNum;
			}
		}
	}

}
