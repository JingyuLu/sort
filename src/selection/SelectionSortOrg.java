package selection;


public class SelectionSortOrg {

	public static void main(String[] args) {
		int[] arr={55,3,12,1,65,33,2};
		// before sort
		System.out.println("before sort");
		for(int numArr1:arr){
			System.out.print(numArr1+" ");
		}
		System.out.println();
		System.out.println();
		System.out.println("Sorting process:");
		
		//start sort
		for(int i=0;i<arr.length;i++){
			int numTemp=i;
			for(int j=numTemp+1;j<arr.length;j++){
				//figure out minimum num
				if(arr[j]<arr[numTemp]){
					//record minimum num
					numTemp=j;
				}
			}
			//first round sort finished
			if(i!=numTemp){
				int tempArr=arr[i];
				arr[i]=arr[numTemp];
				arr[numTemp]=tempArr;
			}
			for(int numArrTemp:arr){
				System.out.print(numArrTemp+" ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println("after sort:");
		for(int numArr2:arr){
			System.out.print(numArr2+" ");
		}
	}

}
