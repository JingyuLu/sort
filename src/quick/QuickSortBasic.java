package quick;

public class QuickSortBasic {

	public static void main(String[] args) {
		
		QuickSortBasic qsb=new QuickSortBasic();
		int[] nums= {3,1,6,4,2,5,8,7};
		
		qsb.quickSortBasic(nums);
	}

	private void quickSortBasic(int[] nums) {
		
		quickSortBasic(nums,0,nums.length-1);
	}

	private void quickSortBasic(int[] nums, int start, int end) {
		
		if(start>end) {
			return;
		}
		int left=start;
		int right=end;
		int index=nums[left];
		
		while(left<right) {
			while(left<right&&index<=nums[right]) {
				right--;
			}
			if(left<right) {
				nums[left++]=nums[right];
			}
			while(left<right&&nums[left]<index) {
				left++;
			}
			if(left<right) {
				nums[right--]=nums[left];
			}
		}
		nums[left]=index;
		quickSortBasic(nums, left, nums.length-1);
		quickSortBasic(nums, left+1, end);
	}

}
