package quick;


import java.util.Arrays;

public class QuickSortOrg {

	public static void quickSort(int[] arrayQuick, int lowest, int highest){
		
		int index;
		int i;
		int j;
		if(lowest>highest){
			return;
		}
		i=lowest;
		j=highest;
		index=arrayQuick[i];
		while(i<j){
			while(i<j&&arrayQuick[j]>=index){
				j--;
			}
			if(i<j){
				arrayQuick[i++]=arrayQuick[j];
			}
			while(i<j&&arrayQuick[i]<index){
				i++;
			}
			if(i<j){
				arrayQuick[j--]=arrayQuick[i];
			}
		}
		arrayQuick[i]=index;
		quickSort(arrayQuick, lowest, arrayQuick.length-1);
		quickSort(arrayQuick, i+1, highest);
	}
	
	public static void sort(int[] arrQiuck){
		quickSort(arrQiuck, 0, arrQiuck.length-1);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int array[]={49, 38, 65, 97, 76, 13, 27, 49};
		sort(array);
		System.out.println(Arrays.toString(array));
	}

}
