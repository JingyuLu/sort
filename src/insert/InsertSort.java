package insert;

import java.util.Arrays;

public class InsertSort {

	public static void main(String[] args) {
		
		InsertSort is=new InsertSort();
		int[] nums={3, 5, 2, 4, 1};
		
		is.insertSort(nums);
		System.out.println(Arrays.toString(nums));
	}

	private void insertSort(int[] nums) {
		
		int len=nums.length;
		int tmpNum=0;
		for(int i=1;i<len;i++) {
//			tmpNum=nums[i];
			for(int j=i;j>0&&nums[j]<nums[j-1];j--) {
				tmpNum=nums[j-1];
				nums[j-1]=nums[j];
				nums[j]=tmpNum;
			}
		}
	}

}
