package insert;


import java.util.Arrays;

public class InsertSortOrg {

	public static void insertSort(int[] arrayInsert){
		int insertTemp;
		for(int i=1;i<arrayInsert.length;i++){
			insertTemp=arrayInsert[i];
			int j=i-1;
			while(j>=0&&insertTemp<arrayInsert[j]){
				arrayInsert[j+1]=arrayInsert[j];
				j--;
			}
			arrayInsert[j+1]=insertTemp;
		}
	}
	
	public static void main(String[] args) {
		int[] array={38, 65, 97, 76, 13, 27, 49};
		insertSort(array);
		System.out.println(Arrays.toString(array));

	}

}
