package bubble;

import java.util.Arrays;

public class BubbleSortOrg {
	
	public static void bubbleSort(int[] bubbleArray){
		
		int temp;
		for(int i=0;i<bubbleArray.length-1;++i){
			for(int j=bubbleArray.length-1;j>i;--j){
				if(bubbleArray[j]<bubbleArray[j-1]){
					temp=bubbleArray[j];
					bubbleArray[j]=bubbleArray[j-1];
					bubbleArray[j-1]=temp;
				}
			}
		}
	}
	
	public static void main(String[] args) {

		int array[] = { 50,38,65,97,76,13,27,50};
        bubbleSort(array);
        System.out.println(Arrays.toString(array));
	}

}
