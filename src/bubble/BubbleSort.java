package bubble;

public class BubbleSort {

	public static void main(String[] args) {
		
		BubbleSort bs=new BubbleSort();
		int[] nums={ 50,38,65,97,76,13,27,50};
		
		bs.bubbleSort(nums);
		for(int i:nums) {
			System.out.print(i+" ");
		}
	}

	private void bubbleSort(int[] nums) {
		
		int len=nums.length;
		int tmpNum=0;
		for(int i=0;i<len-1;i++) {
			for(int j=len-1;j>i;j--) {
				if(nums[j]<nums[j-1]) {
					tmpNum=nums[j];
					nums[j]=nums[j-1];
					nums[j-1]=tmpNum;
				}
			}
		}
	}

}
